# maven-examples

## Overview 

* foo and bar are Maven modules that create Java jars
* foo depends on bar
* bar has no dependencies

## Goals

* Create jars named "Foo.jar" and "Bar.jar"
	* (Desire to match the names of existing ADS Core jar artifacts)
	* Name of the jars is controlled via <finalName> in foo/pom.xml and bar/pom.xml
	
* Create Foo.jar such that it contains the classes provided in Bar.jar
    * (Desire to combine contents of ADS Core jars and ADS Core generated jars)
	* The maven-assembly-plugin accomplishes this in foo/pom.xml
	* <appendAssemblyId>false</appendAssemblyId> avoids creating a second jar
	* The jar will included classes from all dependencies not marked <scope>provided</scope>
	

